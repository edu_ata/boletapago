class AttendanceCard{
    constructor(date,entryTime,departureTime){
        this.date=date;
        this.entryTime=entryTime;
        this.departureTime=departureTime;
    }

    getAmountHours(){
        return (this.departureTime-this.entryTime)/1000;
    }
}

module.exports=AttendanceCard;