class EmployeeForHourSalary {
    constructor(salary,hours){
        this.salary=salary;
        this.hours=hours;
    }
    getSalary(){
        return this.salary*this.hours;
    }
}
module.exports=EmployeeForHourSalary