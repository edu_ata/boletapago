import EmployeeForHourSalary from './employeeForHour';
import EmployeeFixedSalary from './employeeFixed';

class Employee {
    constructor(ci, name, typeEmployee, salary, hours, commissionPercentage, sales) {
        this.ci = ci;
        this.name = name;
        this.typeEmployee = typeEmployee;
        this.salary = salary;
        this.hours = hours;
        this.commissionPercentage = commissionPercentage;
        this.sales = sales;
        this.calculateSalary = this.createCalculateSalary();
    }

    getSalary() {
        return this.calculateSalary.getSalary();
    }

    createCalculateSalary() {
        switch (this.typeEmployee) {
            case 'FIJO':
                return new EmployeeFixedSalary(this.salary);
            case 'POR_HORA':
                return new EmployeeForHourSalary(this.salary, this.hours);
        }
        throw new Error("Should be unreachable");
    }
}

module.exports = Employee;