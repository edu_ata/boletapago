var expect = require('chai').expect

import PaymentBill from '../paymentBill';
import Employee from '../employee';
import EmployeeFixedSalary from '../employeeFixed';

describe('boletas de pago',function(){
    it('generar la boleta de pago para un empleado fijo',function(){
        let employee = new Employee(1,"Erick",'FIJO',500,100,0,0);
        let paymentbill = new PaymentBill(employee);
        expect(paymentbill.calculatePayment()).equal(500);
    });

    it('generar la boleta de pago para un empleado por hora',function(){
        let employee = new Employee(2,"Alavaro","POR_HORA",40,3,0,0);
        let paymentbill = new PaymentBill(employee);
        expect(paymentbill.calculatePayment()).equal(120);
    });
});